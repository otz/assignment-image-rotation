#include "formats/format_bmp.h"
#include "utils/file_utils.h"
#include "utils/format_utils.h"
#include "utils/math_utils.h"

#define BMP_SIGNATURE_WORD 0x4D42
#define BMP_FILE_HEADER_SIZE 14
#define BMP_DIB_INFO_HEADER_SIZE 40
#define BMP_RESERVED_PLACEHOLDER 0
#define BMP_RESOLUTION_PLACEHOLDER 0
#define BMP_PLANES_NUMBER 1
#define BMP_REQUIRED_ALL_COLORS 0

// Legacy bmp header, used by most applications for compatibility reasons
struct bmp_header_layout {
    // Bitmap file header - stores general information about the bitmap image file
    uint8_t bfType[2];          // file type; must be set to the signature word BM (0x4D42 in LE) to indicate bitmap
    uint8_t bfSize[4];          // size, in bytes, of the bitmap file
    uint8_t bfReserved1[2];     // reserved; set to zero
    uint8_t bfReserved2[2];     // reserved; set to zero
    uint8_t bfOffBits[4];       // offset, in bytes, from the bmp_file_header structure to the bitmap bits
    // DIB header - stores detailed information about the bitmap image and defines the pixel format
    uint8_t biSize[4];          // number of bytes required by this version of DIB header (40 for this version)
    uint8_t biWidth[4];         // width of the bitmap, in pixels, signed number
    uint8_t biHeight[4];        // height of the bitmap, in pixels, negative value indicates reverse order of rows
    uint8_t biPlanes[2];        // number of planes for the target device; must be set to 1
    uint8_t biBitCount[2];      // number of bits-per-pixel, possible values are 1, 4, 8, 16, 24 and 32
    uint8_t biCompression[4];   // type of compression_method
    uint8_t biSizeImage[4];     // size, in bytes, of the image; may be set to 0 when no compression_method is applied
    uint8_t biXPelsPerMeter[4]; // horizontal resolution, in pixels-per-meter, signed number
    uint8_t biYPelsPerMeter[4]; // vertical resolution, in pixels-per-meter, signed number
    uint8_t biClrUsed[4];       // number of color indexes in the color table that are actually used by the bitmap
    uint8_t biClrImportant[4];  // number of color indexes that are required for displaying the bitmap; usually ignored
};

/* IO functions
 * Yes, it looks ugly, but there is no other platform-independent way to read the header
 * q.v. compiler-dependent directives, big/middle endianness and unaligned access tolerance */

static struct bmp_header bmp_header_decode(const struct bmp_header_layout *buffer) {
    return (struct bmp_header) {
            .signature = decode_le_u16(buffer->bfType),
            .file_size = decode_le_u32(buffer->bfSize),
            .reserved_1 = decode_le_u16(buffer->bfReserved1),
            .reserved_2 = decode_le_u16(buffer->bfReserved2),
            .data_offset = decode_le_u32(buffer->bfOffBits),
            .dib_header_size = decode_le_u32(buffer->biSize),
            .image_width = decode_le_i32(buffer->biWidth),
            .image_height = decode_le_i32(buffer->biHeight),
            .image_planes = decode_le_u16(buffer->biPlanes),
            .pixel_format = decode_le_u16(buffer->biBitCount),
            .compression_method = decode_le_u32(buffer->biCompression),
            .image_size = decode_le_u32(buffer->biSizeImage),
            .image_X_resolution = decode_le_i32(buffer->biXPelsPerMeter),
            .image_Y_resolution = decode_le_i32(buffer->biYPelsPerMeter),
            .palette_size = decode_le_u32(buffer->biClrUsed),
            .colors_required = decode_le_u32(buffer->biClrImportant),
    };
}

static struct bmp_header_layout bmp_header_encode(const struct bmp_header *header) {
    struct bmp_header_layout buffer;
    encode_le_u16(buffer.bfType, header->signature);
    encode_le_u32(buffer.bfSize, header->file_size);
    encode_le_u32(buffer.bfReserved1, header->reserved_1);
    encode_le_u32(buffer.bfReserved2, header->reserved_2);
    encode_le_u32(buffer.bfOffBits, header->data_offset);
    encode_le_u32(buffer.biSize, header->dib_header_size);
    encode_le_i32(buffer.biWidth, header->image_width);
    encode_le_i32(buffer.biHeight, header->image_height);
    encode_le_u16(buffer.biPlanes, header->image_planes);
    encode_le_u16(buffer.biBitCount, header->pixel_format);
    encode_le_u32(buffer.biCompression, header->compression_method);
    encode_le_u32(buffer.biSizeImage, header->image_size);
    encode_le_i32(buffer.biXPelsPerMeter, header->image_X_resolution);
    encode_le_i32(buffer.biYPelsPerMeter, header->image_Y_resolution);
    encode_le_u32(buffer.biClrUsed, header->palette_size);
    encode_le_u32(buffer.biClrImportant, header->colors_required);
    return buffer;
}

bool bmp_header_read_from_file(FILE *in, struct bmp_header *header) {
    struct bmp_header_layout buffer;
    if (!file_read_chunk(in, sizeof buffer, &buffer)) return false;
    *header = bmp_header_decode(&buffer);
    return true;
}

bool bmp_header_write_to_file(FILE *out, const struct bmp_header *header) {
    struct bmp_header_layout buffer = bmp_header_encode(header);
    return file_write_chunk(out, sizeof buffer, &buffer);
}

/* Validity check functions */

bool bmp_header_signature_is_valid(const struct bmp_header *header) {
    return header->signature == BMP_SIGNATURE_WORD;
}

bool bmp_header_is_rgb24(const struct bmp_header *header) {
    return header->compression_method == BMP_COMPRESSION_RGB && header->pixel_format == BMP_PIXEL_24;
}

static bool bmp_pixel_array_rgb24_size_calculate(uint32_t image_height, uint32_t image_width, uint32_t *image_size) {
    uint32_t row_bytes;
    if (mul2_u32_overflows(image_width, 3, &row_bytes)) { return false; }
    row_bytes = u32_rounded_to_dword(row_bytes);
    return !mul2_u32_overflows(row_bytes, image_height, image_size);
}

static bool bmp_pixel_array_rgb24_size_is_valid(uint32_t image_height, uint32_t image_width, uint32_t image_size) {
    uint32_t minimum_size;
    return bmp_pixel_array_rgb24_size_calculate(image_height, image_width, &minimum_size)
           && image_size >= minimum_size;
}

bool bmp_header_is_valid(const struct bmp_header *header) {
    /* check fields with predefined values */
    if (header->dib_header_size != BMP_DIB_INFO_HEADER_SIZE) { return false; }
    if (header->image_planes != BMP_PLANES_NUMBER) { return false; }
    /* check signed but positive integers for validity */
    if (header->image_width < 0 ||
        header->image_X_resolution < 0 ||
        header->image_Y_resolution < 0) { return false; }
    /* check file size validity */
    uint32_t min_file_size;
    if (sum2_u32_overflows(header->data_offset, header->image_size, &min_file_size) ||
        header->file_size < min_file_size) { return false; }
    /* consider bmp headers describing pixels in any format other than rgb24 invalid, for the sake of simplicity */
    if (!bmp_header_is_rgb24(header)) { return false; }
    if (!bmp_pixel_array_rgb24_size_is_valid(abs_i32(header->image_height), header->image_width, header->image_size)) {
        return false;
    }
    if (header->palette_size != 0 || header->colors_required != BMP_REQUIRED_ALL_COLORS) {
        return false;
    }
    return true;
}

/* bmp_dimensions encapsulates non-trivial image data size/layout encoding logic */

struct bmp_dimensions bmp_header_get_dimensions(const struct bmp_header *header) {
    return (struct bmp_dimensions) {
            .width = header->image_width,
            .height = abs_i32(header->image_height),
            .is_row_order_reversed = header->image_height < 0
    };
}

bool bmp_header_dimensions_are_valid(const struct bmp_dimensions *dims) {
    const uint32_t max_height = (dims->is_row_order_reversed) ? -(uint32_t) INT32_MIN : INT32_MAX;
    return dims->width <= INT32_MAX && dims->height <= max_height;
}

static int32_t bmp_image_dimensions_encode_height(const struct bmp_dimensions *dims) {
    const int32_t height = (int32_t) dims->height;
    return (dims->is_row_order_reversed) ? -height : height;
}

static int32_t bmp_image_dimensions_encode_width(const struct bmp_dimensions *dims) {
    return (int32_t) dims->width;
}

/* header generation */

bool bmp_header_generate_default_rgb24(struct bmp_header *header, const struct bmp_dimensions *dims) {
    const uint32_t header_size = sizeof(struct bmp_header_layout);
    uint32_t image_size;
    uint32_t file_size;
    if (!bmp_pixel_array_rgb24_size_calculate(dims->height, dims->width, &image_size)) { return false; }
    if (sum2_u32_overflows(header_size, image_size, &file_size)) { return false; }
    *header = (struct bmp_header) {
            .signature = BMP_SIGNATURE_WORD,
            .file_size =  file_size,
            .reserved_1 = BMP_RESERVED_PLACEHOLDER,
            .reserved_2 = BMP_RESERVED_PLACEHOLDER,
            .data_offset = header_size,
            .dib_header_size = BMP_DIB_INFO_HEADER_SIZE,
            .image_width = bmp_image_dimensions_encode_width(dims),
            .image_height = bmp_image_dimensions_encode_height(dims),
            .image_planes = BMP_PLANES_NUMBER,
            .pixel_format = BMP_PIXEL_24,
            .compression_method = BMP_COMPRESSION_RGB,
            .image_size= image_size,
            .image_X_resolution = BMP_RESOLUTION_PLACEHOLDER,
            .image_Y_resolution = BMP_RESOLUTION_PLACEHOLDER,
            .palette_size = 0,
            .colors_required = BMP_REQUIRED_ALL_COLORS,
    };
    return true;
}

uint8_t bmp_pixel_row_rgb24_padding_bytes(uint32_t width) {
    const uint8_t dword_remainder = (uint64_t) (width * 3) % 4;
    return (dword_remainder == 0) ? 0 : (4 - dword_remainder);
}
