#include <utils/math_utils.h>

/* size_t math */

extern inline bool mul2_usize_overflows(size_t a, size_t b, size_t *result) ;

extern inline bool is_valid_matrix_size(size_t rows, size_t columns, size_t *index) ;

extern inline size_t matrix_index(size_t row, size_t column, size_t row_size) ;

/* (u)int32_t math */

extern inline uint32_t abs_i32(int32_t x) ;

extern inline uint32_t u32_rounded_to_dword(uint32_t x) ;

extern inline bool sum2_u32_overflows(uint32_t a, uint32_t b, uint32_t *result) ;

extern inline bool mul2_u32_overflows(uint32_t left, uint32_t right, uint32_t *result);
