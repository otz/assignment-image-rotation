#include "utils/file_utils.h"

extern inline bool file_read_chunk(FILE *file, size_t size, void *dest);

extern inline bool file_write_chunk(FILE *file, size_t size, void *src);
