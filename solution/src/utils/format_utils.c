#include <utils/format_utils.h>

extern inline uint16_t decode_le_u16(const uint8_t *data);

extern inline uint32_t decode_le_u32(const uint8_t *data);

extern inline int16_t decode_le_i16(const uint8_t *data);

extern inline int32_t decode_le_i32(const uint8_t *data);

extern inline void encode_le_u16(uint8_t *data, uint16_t value);

extern inline void encode_le_u32(uint8_t *data, uint32_t value);

extern inline void encode_le_i16(uint8_t *data, int16_t value);

extern inline void encode_le_i32(uint8_t *data, int32_t value);
