#include "image/image.h"
#include "converters/image_convert.h"
#include "converters/image_convert_bmp.h"
#include "transformations/image_rotate.h"
#include <stdlib.h>

static void report_error_and_quit(const char *message) {
    fprintf(stderr, "%s", message);
    exit(EXIT_FAILURE);
}

int main(int argc, char **argv) {
    if (argc != 3) {
        report_error_and_quit("Usage: ./image-transformer <load_result-image> <transformed-image>");
    }
    const struct image_load_result load_result = image_load_from_file(argv[1], image_read_bmp_rgb24);
    if (load_result.status != IMAGE_READ_OK) {
        report_error_and_quit("Read error");
    }
    struct image *source = load_result.image;
    struct image *transformed = image_rotate_left90(source);
    image_drop(source);
    if (transformed == NULL) {
        report_error_and_quit("Transform error");
    }
    enum image_write_status dump_status = image_dump_to_file(argv[2], transformed, image_write_to_bmp_rgb24);
    image_drop(transformed);
    if (dump_status != IMAGE_WRITE_OK) {
        report_error_and_quit("Write error");
    }
    fprintf(stdout, "DONE");
    return 0;
}
