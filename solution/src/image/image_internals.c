#include "image/image_internals.h"
#include <malloc.h>

struct pixel_rgb24 *pixel_rgb24_buffer_empty() {
    return NULL;
}

bool pixel_rgb24_buffer_resize(struct pixel_rgb24 **buffer, size_t new_size) {
    if (new_size == 0) {
        pixel_rgb24_buffer_clear(buffer);
        return true;
    }
    struct pixel_rgb24 *new_buffer = reallocarray(*buffer, sizeof **buffer, new_size);
    if (new_buffer == NULL) return false;
    *buffer = new_buffer;
    return true;
}

void pixel_rgb24_buffer_clear(struct pixel_rgb24 **buffer) {
    pixel_rgb24_buffer_drop(buffer);
    *buffer = pixel_rgb24_buffer_empty();
}

void pixel_rgb24_buffer_drop(struct pixel_rgb24 **buffer) {
    free(*buffer);
}
