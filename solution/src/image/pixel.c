#include <image/pixel.h>

extern inline uint8_t pixel_rgb_24_get_red(struct pixel_rgb24 p);

extern inline uint8_t pixel_rgb_24_get_green(struct pixel_rgb24 p);

extern inline uint8_t pixel_rgb_24_get_blue(struct pixel_rgb24 p);
