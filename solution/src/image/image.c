#include "image/image.h"
#include "image/image_internals.h"
#include "utils/math_utils.h"
#include <malloc.h>

struct image {
    size_t height, width;
    struct pixel_rgb24 *buffer;
};

struct image *image_new() {
    struct image *img = malloc(sizeof(struct image));
    if (img == NULL) return NULL;
    img->height = img->width = 0;
    img->buffer = pixel_rgb24_buffer_empty();
    return img;
}

struct image *image_new_sized(size_t height, size_t width) {
    struct image *img = image_new();
    if (img == NULL) return NULL;
    if (!image_resize(img, height, width)) {
        image_drop(img);
        return NULL;
    }
    return img;
}

void image_drop(struct image *img) {
    pixel_rgb24_buffer_drop(&img->buffer);
    free(img);
}


size_t image_get_height(const struct image *img) {
    return img->height;
}

size_t image_get_width(const struct image *img) {
    return img->width;
}

bool image_resize(struct image *img, size_t height, size_t width) {
    size_t new_buffer_size;
    if (!is_valid_matrix_size(height, width, &new_buffer_size)) return false;
    if (!pixel_rgb24_buffer_resize(&img->buffer, new_buffer_size)) return false;
    img->height = height;
    img->width = width;
    return true;
}

struct pixel_rgb24 image_get_pixel_rgb24(const struct image *img, size_t height, size_t width) {
    const size_t index = matrix_index(height, width, img->width);
    return img->buffer[index];
}

void image_set_pixel_rgb24(const struct image *img, struct pixel_rgb24 pxl, size_t height, size_t width) {
    const size_t index = matrix_index(height, width, img->width);
    img->buffer[index] = pxl;
}

void image_copy_pixel(const struct image *source, size_t s_height, size_t s_width,
                      struct image *destination, size_t d_height, size_t d_width) {
    const struct pixel_rgb24 pixel = image_get_pixel_rgb24(source, s_height, s_width);
    image_set_pixel_rgb24(destination, pixel, d_height, d_width);
}
