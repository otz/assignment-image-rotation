#include "transformations/image_rotate.h"
#include <stddef.h>

struct image *image_rotate_left90(const struct image *source) {
    const size_t source_height = image_get_height(source);
    const size_t source_width = image_get_width(source);
    struct image *target = image_new_sized(source_width, source_height);
    if (target == NULL) return NULL;
    for (size_t y = 0; y < source_height; ++y) {
        for (size_t x = 0; x < source_width; ++x) {
            image_copy_pixel(source, y, x,
                             target, x, (source_height - 1) - y);
        }
    }
    return target;
}
