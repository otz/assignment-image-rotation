#include "converters/image_convert_bmp.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "converters/image_convert.h"
#include "formats/format_bmp.h"
#include "utils/file_utils.h"


static bool image_resize_with_bmp_dimension(struct image *img, struct bmp_dimensions *dims) {
    const uint32_t height = dims->height;
    const uint32_t width = dims->width;
    if (SIZE_MAX < UINT32_MAX) {
        if ((uint32_t) SIZE_MAX < height ||
            (uint32_t) SIZE_MAX < width) { return false; }
    }
    return image_resize(img, height, width);
}

static bool bmp_dimensions_from_image(struct bmp_dimensions *dims, const struct image *img) {
    const size_t height = image_get_height(img);
    const size_t width = image_get_width(img);
    /* Note: according to the standard, SIZE_MAX is guaranteed to be at least 2^16 */
    if (UINT32_MAX < SIZE_MAX) {
        if ((size_t) UINT32_MAX < height ||
            (size_t) UINT32_MAX < width) { return false; }
    }
    *dims = (struct bmp_dimensions) {
            .width = (uint32_t) width,
            .height = (uint32_t) height,
            .is_row_order_reversed = false};
    return bmp_header_dimensions_are_valid(dims);
}

/* Read/write bmp_pixel_rgb24 helper functions */

static bool image_read_bmp_pixel_rgb24(FILE *in, struct image *img, uint32_t row, uint32_t column) {
    struct pixel_rgb24 pixel;
    if (!file_read_chunk(in, sizeof pixel, &pixel)) { return false; }
    image_set_pixel_rgb24(img, pixel, row, column);
    return true;
}

static bool image_write_to_bmp_pixel_rgb24(FILE *out, const struct image *img, uint32_t row, uint32_t column) {
    struct pixel_rgb24 pixel = image_get_pixel_rgb24(img, row, column);
    return file_write_chunk(out, sizeof pixel, &pixel);
}

/* Read/write nmp_pixel_row_rgb24 helper functions */

static bool image_read_bmp_pixel_row_rgb24(FILE *in, struct image *img, uint32_t row, uint32_t width) {
    for (uint32_t column = 0; column < width; ++column) {
        if (!image_read_bmp_pixel_rgb24(in, img, row, column)) { return false; }
    }
    const uint8_t padding_bytes = bmp_pixel_row_rgb24_padding_bytes(width);
    return (fseek(in, padding_bytes, SEEK_CUR) != -1);
}

static bool image_write_to_bmp_pixel_row_rgb24(FILE *out, const struct image *img, uint32_t row, uint32_t width) {
    for (uint32_t column = 0; column < width; ++column) {
        if (!image_write_to_bmp_pixel_rgb24(out, img, row, column)) { return false; }
    }
    const uint8_t padding_bytes = bmp_pixel_row_rgb24_padding_bytes(width);
    return (fseek(out, padding_bytes, SEEK_CUR) != -1);
}

/* Read/write bmp_pixel_array_rgb24 helper functions */

static bool image_read_bmp_pixel_array_rgb24(FILE *in, struct image *img, const struct bmp_dimensions *dims) {
    int64_t start = 0, step = 1, end = dims->height;
    if (dims->is_row_order_reversed) {
        start = end - 1;
        step = end = -1;
    }
    for (int64_t row = start; row != end; row += step) {
        if (!image_read_bmp_pixel_row_rgb24(in, img, row, dims->width)) { return false; }
    }
    return true;
}

static bool image_write_bmp_pixel_array_rgb24(FILE *out, const struct image *img, const struct bmp_dimensions *dims) {
    if (dims->is_row_order_reversed) { return false; }
    for (int64_t row = 0; row < dims->height; ++row) {
        if (!image_write_to_bmp_pixel_row_rgb24(out, img, row, dims->width)) { return false; }
    }
    return true;
}

enum image_read_status image_read_bmp_rgb24(FILE *in, struct image *img) {
    struct bmp_header header;
    if (!bmp_header_read_from_file(in, &header)) { return image_read_failure_cause(in); }
    if (!bmp_header_signature_is_valid(&header)) { return IMAGE_READ_INVALID_SIGNATURE; }
    if (!bmp_header_is_valid(&header) ||
        !bmp_header_is_rgb24(&header)) { return IMAGE_READ_INVALID_HEADER; }
    struct bmp_dimensions dims = bmp_header_get_dimensions(&header);
    if (!image_resize_with_bmp_dimension(img, &dims)) { return IMAGE_READ_INTERNAL_ERROR; }
    if (fseek(in, header.data_offset, SEEK_SET) == -1) { return IMAGE_READ_IO_ERROR; }
    if (!image_read_bmp_pixel_array_rgb24(in, img, &dims)) { return image_read_failure_cause(in); }
    return IMAGE_READ_OK;
}

enum image_write_status image_write_to_bmp_rgb24(FILE *out, const struct image *img) {
    struct bmp_dimensions dims;
    if (!bmp_dimensions_from_image(&dims, img)) { return IMAGE_WRITE_INVALID_IMAGE; };
    struct bmp_header header;
    if (!bmp_header_generate_default_rgb24(&header, &dims)) { return IMAGE_WRITE_INVALID_IMAGE; }
    if (!bmp_header_write_to_file(out, &header)) { return IMAGE_WRITE_IO_ERROR; }
    if (fseek(out, header.data_offset, SEEK_SET) == -1) { return IMAGE_WRITE_IO_ERROR; }
    if (!image_write_bmp_pixel_array_rgb24(out, img, &dims)) { return IMAGE_WRITE_IO_ERROR; }
    return IMAGE_WRITE_OK;
}
