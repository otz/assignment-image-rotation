#include "converters/image_convert.h"
#include <stdio.h>

extern inline enum image_read_status image_read_failure_cause(FILE *file);

extern inline struct image_load_result image_load_result_success(struct image *img);

extern inline struct image_load_result image_load_result_error(enum image_read_status status);

static struct image_load_result image_load_from_stream(FILE *in, image_reader *reader) {
    struct image *img = image_new();
    if (img == NULL) { return image_load_result_error(IMAGE_READ_INTERNAL_ERROR); }
    const enum image_read_status status = reader(in, img);
    if (status != IMAGE_READ_OK) {
        image_drop(img);
        return image_load_result_error(status);
    }
    return image_load_result_success(img);
}

struct image_load_result image_load_from_file(const char *filename, image_reader *reader) {
    FILE *in = fopen(filename, "rb");
    if (in == NULL) {
        return image_load_result_error(IMAGE_READ_IO_ERROR);
    }
    struct image_load_result result = image_load_from_stream(in, reader);
    if (fclose(in) == EOF) {
        return image_load_result_error(IMAGE_READ_IO_ERROR);
    }
    return result;
}

enum image_write_status image_dump_to_file(const char *filename, const struct image *img, image_writer *writer) {
    FILE *out = fopen(filename, "wb");
    if (out == NULL) {
        return IMAGE_WRITE_IO_ERROR;
    }
    enum image_write_status status = writer(out, img);
    if (fclose(out) == EOF) {
        return IMAGE_WRITE_IO_ERROR;
    }
    return status;
}
