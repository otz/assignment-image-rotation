#ifndef ASSIGNMENT_IMAGE_ROTATION_FORMAT_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_FORMAT_BMP_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

// Legacy bmp header, used by most applications for compatibility reasons
struct bmp_header {
    uint16_t signature;
    uint32_t file_size;
    uint16_t reserved_1;
    uint16_t reserved_2;
    uint32_t data_offset;
    uint32_t dib_header_size;
    int32_t image_width;
    int32_t image_height;
    uint16_t image_planes;
    uint16_t pixel_format;
    uint32_t compression_method;
    uint32_t image_size;
    int32_t image_X_resolution;
    int32_t image_Y_resolution;
    uint32_t palette_size;
    uint32_t colors_required;
};

enum bmp_dib_header_size {
    BMP_DIB_INFO_HEADER_SIZE = 40,
    BMP_DIB_V4_HEADER_SIZE = 108,
    BMP_DIB_V5_HEADER_SIZE = 124,
};

enum bmp_compression {
    BMP_COMPRESSION_RGB = 0,
    BMP_COMPRESSION_RLE8 = 1,
    BMP_COMPRESSION_RLE4 = 2,
    BMP_COMPRESSION_BITFIELDS = 3,
    BMP_COMPRESSION_JPEG = 4,
    BMP_COMPRESSION_PNG = 5,
};

enum bmp_pixel {
    BMP_PIXEL_UNDEFINED = 0,
    BMP_PIXEL_MONOCHROME = 1,
    BMP_PIXEL_HALFBYTE = 4,
    BMP_PIXEL_BYTE = 8,
    BMP_PIXEL_WORD = 16,
    BMP_PIXEL_24 = 24,
    BMP_PIXEL_DWORD = 32,
};

struct bmp_dimensions {
    uint32_t width, height;
    bool is_row_order_reversed;
};

bool bmp_header_read_from_file(FILE *in, struct bmp_header *header);

bool bmp_header_write_to_file(FILE *out, const struct bmp_header *header);

bool bmp_header_signature_is_valid(const struct bmp_header *header);

bool bmp_header_is_valid(const struct bmp_header *header);

bool bmp_header_is_rgb24(const struct bmp_header *header);

struct bmp_dimensions bmp_header_get_dimensions(const struct bmp_header *header);

bool bmp_header_dimensions_are_valid(const struct bmp_dimensions *dims);

bool bmp_header_generate_default_rgb24(struct bmp_header *header, const struct bmp_dimensions *dims);

uint8_t bmp_pixel_row_rgb24_padding_bytes(uint32_t width);

#endif //ASSIGNMENT_IMAGE_ROTATION_FORMAT_BMP_H
