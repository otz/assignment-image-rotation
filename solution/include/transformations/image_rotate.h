#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_ROTATE_H

#include "image/image.h"

// May return NULL on allocation error
struct image *image_rotate_left90(const struct image *source);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_ROTATE_H
