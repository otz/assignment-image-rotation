#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_CONVERT_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_CONVERT_H

#include <stdio.h>
#include "image/image.h"

enum image_read_status {
    IMAGE_READ_OK = 0,
    IMAGE_READ_IO_ERROR,
    IMAGE_READ_INTERNAL_ERROR,
    IMAGE_READ_INVALID_SIGNATURE,
    IMAGE_READ_INVALID_HEADER,
    IMAGE_READ_UNEXPECTED_EOF,
};

enum image_write_status {
    IMAGE_WRITE_OK = 0,
    IMAGE_WRITE_IO_ERROR,
    IMAGE_WRITE_INVALID_IMAGE,
};

struct image_load_result {
    enum image_read_status status;
    struct image *image;
};

inline enum image_read_status image_read_failure_cause(FILE* file) {
    if (ferror(file)) return IMAGE_READ_IO_ERROR;
    if (feof(file)) return IMAGE_READ_UNEXPECTED_EOF;
    return IMAGE_READ_INTERNAL_ERROR;
}

inline struct image_load_result image_load_result_success(struct image *img) {
    return (struct image_load_result) {.status=IMAGE_READ_OK, .image=img};
}

inline struct image_load_result image_load_result_error(enum image_read_status status) {
    return (struct image_load_result) {.status=status, .image=NULL};
}

typedef enum image_read_status image_reader(FILE *in, struct image *img);

typedef enum image_write_status image_writer(FILE *out, const struct image *img);

struct image_load_result image_load_from_file(const char *filename, image_reader *reader);

enum image_write_status image_dump_to_file(const char *filename, const struct image *img, image_writer *writer);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_CONVERT_H
