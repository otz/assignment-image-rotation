#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_CONVERT_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_CONVERT_BMP_H

#include <stdio.h>
#include "converters/image_convert.h"

// Both reader and writer expect pointer arguments not to be NULL

enum image_read_status image_read_bmp_rgb24(FILE *in, struct image *img);

enum image_write_status image_write_to_bmp_rgb24(FILE *out, const struct image *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_CONVERT_BMP_H
