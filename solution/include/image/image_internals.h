#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_INTERNALS_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_INTERNALS_H

#include <stdbool.h>
#include <sys/types.h>
#include "pixel.h"

struct pixel_rgb24 *pixel_rgb24_buffer_empty();

bool pixel_rgb24_buffer_resize(struct pixel_rgb24 **buffer, size_t new_size);

void pixel_rgb24_buffer_clear(struct pixel_rgb24 **buffer);

void pixel_rgb24_buffer_drop(struct pixel_rgb24 **buffer);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_INTERNALS_H
