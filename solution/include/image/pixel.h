#ifndef ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
#define ASSIGNMENT_IMAGE_ROTATION_PIXEL_H

#include <stdint.h>

struct pixel_rgb24 {
    uint8_t data[3];
};

inline uint8_t pixel_rgb_24_get_red(struct pixel_rgb24 p) {
    return p.data[2];
}

inline uint8_t pixel_rgb_24_get_green(struct pixel_rgb24 p) {
    return p.data[1];
}

inline uint8_t pixel_rgb_24_get_blue(struct pixel_rgb24 p) {
    return p.data[0];
}

#endif //ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
