#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>

#include "pixel.h"

struct image;

// All image *constructors* may return NULL on allocation error
struct image *image_new();

struct image *image_new_sized(size_t height, size_t width);

// Image *destructor* can handle NULL pointer
void image_drop(struct image *img);

// All image *methods* that take a pointer to an image as an argument expect it to be non-NULL
size_t image_get_height(const struct image *img);

size_t image_get_width(const struct image *img);

bool image_resize(struct image *img, size_t height, size_t width);

// All image *getters* expect indexes to be valid and data on those indexes to be initialized
// Note: the convention is that the indexing is relative to the lower left corner of the image
struct pixel_rgb24 image_get_pixel_rgb24(const struct image *img, size_t height, size_t width);

// All image *setters* expect indexes to be valid
// Note: the convention is that the indexing is relative to the lower left corner of the image
void image_set_pixel_rgb24(const struct image *img, struct pixel_rgb24 pxl, size_t height, size_t width);

void image_copy_pixel(const struct image *source, size_t s_height, size_t s_width,
                      struct image *destination, size_t d_height, size_t d_width);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
