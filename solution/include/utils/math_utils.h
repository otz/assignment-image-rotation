#ifndef ASSIGNMENT_IMAGE_ROTATION_MATH_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_MATH_UTILS_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

inline bool mul2_usize_overflows(size_t a, size_t b, size_t *result) {
    *result = a * b;
    // Note: straightforward way, might be slow due to division
    return (a != 0) && (*result / a != b);
}

inline bool is_valid_matrix_size(size_t rows, size_t columns, size_t *index) {
    return !mul2_usize_overflows(rows, columns, index);
}

inline size_t matrix_index(size_t row, size_t column, size_t row_size) {
    return row * row_size + column;
}

/* (u)int32_t math */

inline uint32_t abs_i32(int32_t x) {
    return (x >= 0) ? x : -x;
}

inline uint32_t u32_rounded_to_dword(uint32_t x) {
    const uint32_t remainder = x % 4;
    return (remainder == 0) ? x : x + (4 - remainder);
}

inline bool sum2_u32_overflows(uint32_t a, uint32_t b, uint32_t *result) {
    const uint64_t u64_result = (uint64_t) a +  (uint64_t) b;
    *result = (uint32_t) u64_result;
    bool r = u64_result > (uint64_t) UINT32_MAX;
    return r == true;
}

inline bool mul2_u32_overflows(uint32_t left, uint32_t right, uint32_t *result) {
    // Note: probably extremely slow on 32bit architecture due to quadword multiplication
    const uint64_t u64_result = (uint64_t) left * (uint64_t) right;
    *result = (uint32_t) u64_result;
    return u64_result > (uint64_t) UINT32_MAX;
}

#endif //ASSIGNMENT_IMAGE_ROTATION_MATH_UTILS_H
