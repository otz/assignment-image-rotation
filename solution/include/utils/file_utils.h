#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H

#include <stdbool.h>
#include <stdio.h>

inline bool file_read_chunk(FILE *file, size_t size, void *dest) {
    return fread(dest, size, 1, file) == 1;
}

inline bool file_write_chunk(FILE *file, size_t size, void *src) {
    return fwrite(src, size, 1, file) == 1;
}

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H
