#ifndef ASSIGNMENT_IMAGE_ROTATION_UTILS_FORMAT_H
#define ASSIGNMENT_IMAGE_ROTATION_UTILS_FORMAT_H

#include <stdint.h>

inline uint16_t decode_le_u16(const uint8_t *data) {
    return (uint16_t) data[0] | (uint16_t) data[1] << 8;
}

inline uint32_t decode_le_u32(const uint8_t *data) {
    return (uint32_t) data[0] | (uint32_t) data[1] << 8 |
           (uint32_t) data[2] << 16 | (uint32_t) data[3] << 24;
}

inline int16_t decode_le_i16(const uint8_t *data) {
    return (int16_t) decode_le_u16(data);
}

inline int32_t decode_le_i32(const uint8_t *data) {
    return (int32_t) decode_le_u32(data);
}

inline void encode_le_u16(uint8_t *data, uint16_t value) {
    data[0] = (uint8_t) (value & 0xFF);
    data[1] = (uint8_t) (value >> 8 & 0xFF);
}

inline void encode_le_u32(uint8_t *data, uint32_t value) {
    data[0] = (uint8_t) (value & 0xFF);
    data[1] = (uint8_t) (value >> 8 & 0xFF);
    data[2] = (uint8_t) (value >> 16 & 0xFF);
    data[3] = (uint8_t) (value >> 24 & 0xFF);
}

inline void encode_le_i16(uint8_t *data, int16_t value) {
    encode_le_u16(data, (uint16_t) value);
}

inline void encode_le_i32(uint8_t *data, int32_t value) {
    encode_le_u32(data, (uint32_t) value);
}

#endif //ASSIGNMENT_IMAGE_ROTATION_UTILS_FORMAT_H
